﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace BoggleBoardWeb
{
    public partial class BoggleBoard : Page
    {
        static int width_ = 4;
        static int desiredLength_ = 3;
        static int desiredSeed_ = 7;
        static int divisorHeight = 0;
        static int divisorWidth = 0;
        static int scoreComp;
        static int scoreHuman;
        static Queue<Tuple<string, char>> stringValues = new Queue<Tuple<string, char>>();
        static BogglePieceGenerator generator = new BogglePieceGenerator(width_, desiredSeed_);
        static BoggleBoard_Board boardInformation = new BoggleBoard_Board(width_, generator);
        static List<string> acceptedWords = new List<string>();
        static private char[,] board2D = new char[width_, width_];
        static Lexicon lexi = new Lexicon("/App_Start/lexicon-clean.txt");
        //TextBox humanScoreDisplay = new TextBox();
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
                //only call if this is the first entry.
                fillBoard();
                computerScoreDisplay.Text = scoreComp.ToString();
                humanScoreDisplay.Text = scoreHuman.ToString();
           }
            while (this.Panel1.Height.Value % width_ != 0)
            {
                this.Panel1.Height = new Unit(this.Panel1.Height.Value - 1);
            }
            //get the divisor
            divisorHeight = (int)this.Panel1.Height.Value / width_;
            divisorHeight -= 2;
            while (this.Panel1.Width.Value % width_ != 0)
            {
                this.Panel1.Width = new Unit(this.Panel1.Width.Value - 1);
            }
            divisorWidth = (int)this.Panel1.Width.Value / width_;
            divisorWidth -= 2;
            gridLayout();
            computerScoreDisplay.Text = scoreComp.ToString();
            humanScoreDisplay.Text = scoreHuman.ToString();
        }
        //makes the board to be used by the gridLayout to set all the values.
        private char[,] fillBoard()
        {
            //Panel playArea = new Panel();
            //playArea = (Panel)FindControl("playArea");
            for (int x = 0; x < width_; x++)
            {
                for (int y = 0; y < width_; y++)
                {
                    board2D[x, y] = boardInformation.getLetter(x, y);
                }
            }
            return board2D;
        }
        public BoggleBoard()
        {
            
            
            
        }
        //this makes the grid layout for the program. 
        public void gridLayout()
        {
            Button[,] grid = new Button [width_,width_]; //for width * height
            for(int height = 0; height < width_; height++)
            {
                for(int width = 0; width < width_; width++)
                {
                    grid[width, height] = new Button();
                    grid[width, height].Text = board2D[width,height].ToString();
                    grid[width, height].ID = width.ToString() + '_' + height.ToString() + '_' + "btn";
                    grid[width, height].Width = divisorWidth;
                    grid[width, height].Height = divisorHeight;
                    grid[width, height].BackColor = System.Drawing.Color.White;
                    grid[width, height].Visible = true;
                    grid[width, height].Click += new System.EventHandler(OnClick);
                    this.Panel1.Controls.Add(grid[width, height]);
                }
            }
            //this.Controls.Add(playArea);
        }
        //if the user clicks on a white square it turns red
        //else it turns red for 'selected'
        protected void OnClick(object sender, EventArgs e)
        {
            //Panel playArea = (Panel)FindControl("playArea");
            Button temp = sender as Button;
            if( temp.BackColor == System.Drawing.Color.White)
            {
                temp.BackColor = System.Drawing.Color.Red;
                stringValues.Enqueue(new Tuple<string, char>(temp.ID, temp.Text.ToCharArray()[0]));
            }
            else
            {
                //check to see if there is a way to make this dynamic so we only change the ones surronding the thing.
                Button button;
                for (int height = 0; height < width_; height++)
                {
                    for (int width = 0; width < width_; width++)
                    {
                        string locator = width.ToString() + '_' + height.ToString() + '_' + "btn";
                        button = (Button)this.Panel1.FindControl(locator.ToString());   
                        button.BackColor = System.Drawing.Color.White; 
                    }
                }
                stringValues.Clear();
            }
        }
        //Check the squares clicked to see if the user has made a word.
        protected void Check_Click(object sender, EventArgs e)
        {
            Panel playArea = (Panel)FindControl("Panel1");
            if (stringValues.Count != 0)
            {
                string stringToBeSent = new string(stringValues.Dequeue().Item2, 1);
                while (stringValues.Count != 0)
                {
                    stringToBeSent += stringValues.Dequeue().Item2;
                }
                if (stringToBeSent.Length >= desiredLength_ && !hasBeenPlayer(stringToBeSent) && lexi.wordStatus(stringToBeSent) == Lexicon.Status.WORD)
                {
                    if (boardInformation.isWordOnBoard(stringToBeSent))
                    {
                        //this.Controls.Add(humanScoreDisplay);
                        acceptedWords.Add(stringToBeSent);
                        this.humanScoreDisplay.Text = acceptedWords.Count.ToString();
                        scoreHuman = acceptedWords.Count;
                    }
                }
                //reset the buttons back to their orignal color
                Button button;
                for (int height = 0; height < width_; height++)
                {
                    for (int width = 0; width < width_; width++)
                    {
                        string locator = width.ToString() + '_' + height.ToString() + '_' + "btn";
                        button = (Button)this.Panel1.FindControl(locator.ToString());
                        button.BackColor = System.Drawing.Color.White;
                    }
                }
            }
        }

        private bool hasBeenPlayer(string wordToBeCheck)
        {
            //bool hasPlayed = false;
            foreach(string word in acceptedWords)
            {
                if(word == wordToBeCheck)
                {
                    return true;
                }
            }
            return false;
        }
       protected void doneBTN_Click(object sender, EventArgs e)
        {
            ComputerPlayer compPlayer = new ComputerPlayer(lexi);
            List<string> computerPlayedWords = compPlayer.playBoggle(boardInformation, acceptedWords, 3);
            this.computerScoreDisplay.Text = computerPlayedWords.Count.ToString();
            scoreComp = computerPlayedWords.Count;
            finishBTN.Visible = true;
        }
        protected void playAgain_Click(object sender, EventArgs e)
       {
            //generator = new BogglePieceGenerator(width_, desiredSeed_);
            boardInformation = new BoggleBoard_Board(width_, generator);
           board2D = new char[width_, width_];
           board2D = fillBoard();
           Button button;
            //remove all the buttons
           for (int height = 0; height < width_; height++)
           {
               for (int width = 0; width < width_; width++)
               {
                   string locator = width.ToString() + '_' + height.ToString() + '_' + "btn";
                   button = (Button)this.Panel1.FindControl(locator.ToString());
                   this.Panel1.Controls.Remove(button);
               }
           }
            //recreate all the buttons
           gridLayout();
           scoreComp = 0;
           scoreHuman = 0;
           acceptedWords.Clear();
           computerScoreDisplay.Text = "0";
           humanScoreDisplay.Text = "0";
       }
    }
}