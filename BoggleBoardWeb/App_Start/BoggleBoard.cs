﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoggleBoardWeb
{
    class BoggleBoard_Board
    {
        public BoggleBoard_Board( int width, BogglePieceGenerator gen )
        {
	        
	        for ( int i = 0; i < width; i++ )
	        {
                List<trieNode> temp = new List<trieNode>();
		        for ( int j = 0; j < width; j++ )
		        {
			        //char tempChar = gen.getNextChar();
                    trieNode tempNode = new trieNode(gen.getNextChar(), new List<trieNode>());
			        temp.Add( tempNode );
		        }
		        board_.Add(temp);
		        //temp.Clear();
	        }
	        width_ = width;
	        for ( int i = 0; i < width; i++ )
	        {
		        for ( int j = 0; j < width; j++ )
		        {
			        setNeighbors(board_[i][j], i, j);
		        }
	        }
        }
        void setNeighbors(trieNode node, int row, int column)
        {
	        if(row + 1 < width_ && column + 1 < width_){
		        node.neighbors_.Add(board_[row + 1][column + 1]);
	        }
	        if(row + 1 < width_){
		        node.neighbors_.Add(board_[row + 1][column]);
	        }
	        if(column + 1 < width_){
		        node.neighbors_.Add(board_[row][column + 1]);
	        }
	        if(row > 0 && column > 0){
		        node.neighbors_.Add(board_[row - 1][column - 1]);
	        }
	        if(row > 0){
		        node.neighbors_.Add(board_[row - 1][column]);
	        }
	        if(column > 0){
		        node.neighbors_.Add(board_[row][column - 1]);
	        }
	        if(row > 0 && column + 1 < width_)
	        {
		        node.neighbors_.Add(board_[row - 1][column + 1]);
	        }
	        if(row + 1 < width_ && column > 0)
	        {
		        node.neighbors_.Add(board_[row + 1][column - 1]);
	        }
        }
        ~BoggleBoard_Board()
        {
	        board_.Clear();
        }
        BoggleBoard_Board(BoggleBoard_Board rhs)
        {
            List<trieNode> temp = new List<trieNode>();
		        for ( int i = 0; i < rhs.width_; i++ )
		        {
			        for ( int j = 0; j < rhs.width_; j++ )
			        {
                        trieNode tempNode = new trieNode(rhs.board_[i][j].getChars(), new List<trieNode>());
				        temp.Add(tempNode);
			        }
			        board_.Add(temp);
			        temp.Clear();
		        }
		         this.width_ = rhs.width_;
		        for ( int i = 0; i < this.width_; i++ )
		        {
			        for ( int j = 0; j < this.width_; j++ )
			        {
				        setNeighbors(board_[i][j].getNode(), i, j);
			        }
		        }
        }
        public bool isWordOnBoard( string word )
        {
	        bool wordOnBoard = false;
	        int i = 0;
	        int j = 0;
	        while( i < width_ && !wordOnBoard )
	        {
		        while( j < width_ && !wordOnBoard)
		        {
			        if(board_[i][j].getChars() == Char.ToUpper(word[0]))
			        {
				        findWord(board_[i][j], ref word, 1, ref wordOnBoard);
			        }
			        j++;
		        }
		        i++;
		        j = 0;
	        }
	        return wordOnBoard;
        }
        public void findWord(trieNode node, ref string word, int letterInt, ref bool wordOnBoard)
        {
            trieNode cursor = node;
	        if(cursor.visited_)
	        {
		        //do nothing fail safe
	        }
	        else if( letterInt >= word.Length)
	        {
		        //if you hit the end of the word set to true and exit
		        wordOnBoard = true;
	        }
	        else 
	        {
		        cursor.visited_ = true;
		        //List<Node>::iterator iter = cursor.neighbors_.begin();
		        foreach(var iter in cursor.neighbors_)
		        {
			        if((iter).getChars() ==  Char.ToUpper(word[letterInt]))
			        {
				        findWord(iter, ref word, letterInt + 1, ref wordOnBoard);
			        }
		        }
	        }
	        //reset at the close of the recursive function
	        cursor.visited_ = false;
        }

        public char getLetter( int row, int column )
        {
	        return board_[ row ][ column ].getChars();
        }
        public trieNode getNode(int row, int column)
        {
	        return board_[ row ][ column ];
        }
        public int getWidth()
        {
	        return width_;
        }
        private List<List<trieNode>> board_ = new List<List<trieNode>>();
        private int width_;

    }
}
