﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoggleBoardWeb
{
    public class BogglePieceGenerator
    {
         
        public BogglePieceGenerator(int boardSize, int rseed){
            index = 0;

              // 1. choose the set of boggle cubes
              if( boardSize <= SMALL_BOARD ){
                  cubes = new List<string>(cube16pattern.AsEnumerable());
              }else{
                cubes = new List<string>(cube25pattern.AsEnumerable());
              }

              // 2. Setup the random number generator
              srand = new Random(rseed);
        }
        // return the next character for the boggle board
        public char getNextChar(){
            // if its the first time we are selecting a cube
            // randomly shuffle the cubes
            if( index == 0 ){
                ShuffleProgram.Shuffle(cubes, srand);
            }

            int cubeSide = srand.Next(CUBE_SIDES);
            char result = cubes[index][cubeSide];

            // look at the next cube
            // handle wrap around
            index++;
            if( index >= cubes.Count ){
            index -= cubes.Count;
            }

            return result;
        }
        // reset the piece generator in preparation 
        // for generating pieces for another board
        public void reSeed(int rseed){
              index = 0;
               srand = new Random(rseed);
        }
      
        private string[] cube16pattern = {
          "aaeegn", "abbjoo", "achops", "affkps",
          "aoottw", "cimotu", "deilrx", "delrvy",
          "distty", "eeghnw", "eeinsu", "ehrtvw", 
          "eiosst", "elrtty", "himnqu", "hlnnrz"
                                       };

         private string[] cube25pattern = {
          "aafirs", "adennn", "aaafrs", "aeegmu", "aaeeee",
          "aeeeem", "afirsy", "aegmnn", "bjkqxz","ceipst", 
          "ceiilt", "ccnstw", "ceilpt", "ddlonr", "dhlnor",
          "dhhlor", "dhhnot", "ensssu", "emottt", "eiiitt", 
          "fiprsy", "gorrvw", "hiprry", "nootuw", "ooottu"
                                       };
        private int index;
        private List<string> cubes;

        // standard boggle cubes for 4x4 and 5x5 boards
        private const int SMALL_BOARD=16; 
        private const int LARGE_BOARD=25; 
        private const int CUBE_SIDES= 6;
        //static const string[] cube16pattern = new string[SMALL_BOARD];
        //static const string[] cube25pattern = new string[LARGE_BOARD];
        Random srand;
    }
}
