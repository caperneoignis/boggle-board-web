﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BoggleBoardWeb
{
    class trieNode
    {
		public char letter_;
		public bool visited_;
		public List<trieNode> neighbors_;
		//std::list<Node*> neighbors_;
		public trieNode( char letter, List<trieNode> listN)
        {
		    letter_ = Char.ToUpper(letter);
            visited_ = false;
            neighbors_= listN;
        }
        ~trieNode()
        { 
          neighbors_.Clear();
        }
        public List<trieNode> getNeighbors()
		{
			return neighbors_;
		}
        public bool Visited
        {
            get { return visited_; }
            set { visited_ = value; }
        }
		//public bool getVisited(){return visited_;}
		//public void setVisited(bool visit){visited_ = visit;}
		public char getChars()
		{
			return letter_;
		}
        public trieNode getNode()
        {
            return this;
        }
		public bool clear()
		{
			//mark this not visited when you exit the function checking words
			visited_ = false;
			return visited_ == false;
		}
    }
}
