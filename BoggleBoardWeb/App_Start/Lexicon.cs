﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Web;
namespace BoggleBoardWeb
{
    class Lexicon
    {
        public Lexicon(string lexiconFile)
        {
          string lexiconEntry;
              StreamReader lexicon;
              lexicon = new StreamReader(HttpContext.Current.Server.MapPath(lexiconFile));
              while ( !lexicon.EndOfStream )
              {
                    lexiconEntry = lexicon.ReadLine();
                    wordMap_.Insert(lexiconEntry.ToUpper());
              }
                lexicon.Close();
        }
        //copy constructor
        public Lexicon(Lexicon rhs)
        {
            wordMap_ = rhs.wordMap_;
        }

        public Lexicon.Status wordStatus(string word)
        {
            Lexicon.Status status = Lexicon.Status.NOT_WORD;
            if( wordMap_.LookUp(word.ToUpper()) != Lexicon.Status.NOT_WORD)
            {
                status = wordMap_.LookUp(word.ToUpper());
            }
	        //return the word status
            return status;
        }
        //this should be obvious
          public enum Status{NOT_WORD, WORD, WORD_PREFIX};
          private Trie wordMap_ = new Trie();
    }
}
