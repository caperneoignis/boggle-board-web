﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoggleBoardWeb
{
    class ComputerPlayer
    {
        public ComputerPlayer(Lexicon lexicon)
        {
	        lexiconFile_ = lexicon;
        }
       /* ~ComputerPlayer()
        {
	
        }*/

        //the play portion
       public List<string> playBoggle(BoggleBoard_Board theBoard, List<string> humanPlayedWords, int desiredSize)
        {
            HashSet<string> addWord = new HashSet<string>();
            //List<string> addWord = new List<string>();
	        for( int row = 0; row < theBoard.getWidth(); row++)
	        {
		        for(int column = 0; column < theBoard.getWidth(); column++)
		        {
                    string potentialWord = new string(theBoard.getLetter(row,column),0);
			        playFunctionHelper(theBoard.getNode(row, column), ref potentialWord, ref addWord, 1, desiredSize);
		        }
	        }
            //need to make this into a set
	        //addWord.Sort();
	        //addWord = addWord.Distinct().ToList();
	        //when I put find inside the recurssive statement it took significatlly longer
	        //find the human words already played and remove
	        foreach(var humanIter in humanPlayedWords)
	        {
		        addWord.Remove(humanIter);
	        }
	        //return truncated and pruned list.
	        return addWord.ToList();
        }
       private void playFunctionHelper(trieNode nodeForNeighbors, ref string potentialWord, ref HashSet<string> addWords, int count, int sizeWanted)
        {
	        if(nodeForNeighbors != null){
                trieNode cursor = nodeForNeighbors;
			        cursor.Visited = true;
			        //List<Node>::iterator iter = cursor->getNeighbors().begin();
			        //add space to word before doing pushback stuff
                    string orignal = potentialWord;
			        //potentialWord += ' ';
			        foreach( var iter in cursor.getNeighbors())
			        {
				        potentialWord += iter.getChars();
				        if( !iter.Visited && lexiconFile_.wordStatus(potentialWord) == Lexicon.Status.WORD){
					        if( potentialWord.Length >= sizeWanted){ //
						        addWords.Add(potentialWord);
					        }
				        }
				        //also continue to recurse
				        if( !iter.Visited && promising(potentialWord)){
				          // cout << potentialWord << endl;
				          playFunctionHelper(iter, ref potentialWord,ref addWords, count + 1, sizeWanted);
				        }
                        potentialWord = orignal; //reset it after appending first letter
			        }
                    potentialWord = orignal;//drop the last row make sure it got changed
		        cursor.Visited = false;
	        }
        }
        private bool promising(string potentialWord)
        {
	        return (lexiconFile_.wordStatus(potentialWord) != Lexicon.Status.NOT_WORD);
        }
        Lexicon lexiconFile_;
    }
}
