﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BoggleBoardWeb
{
    class Trie
    {
        public class LCTrieNode
		{
		  //friend class LCTrie;
			public bool path_ = false;
			public Dictionary<char, LCTrieNode> children_ = new Dictionary<char,LCTrieNode>();
			/*public LCTrieNode(bool incomingTruth)
			{
                path_ = incomingTruth;
                //children_ = new Dictionary<char, LCTrieNode>();
            }*/ 
            //~LCTrieNode(){}
		}	
		
		LCTrieNode root_ = new LCTrieNode();
        //clear trie after use
		private void clear(LCTrieNode node)
        {
			//go through root's childern at first and then call clear
			foreach(var iter in node.children_){
				//if it isn't NULL recurse
					//call on the 
					clear(iter.Value);
				}
		}
		public Trie()
		{
			//root_ = new LCTrieNode(false); //kick it off
		}

		//LCTrie Destructor
		~Trie()
		{
			clear(root_);
		}
		
		//meat and potatoes. 
		public void Insert(string wordedToBeEntered){
			LCTrieNode current = root_;
			//if empty do nothing
			if(wordedToBeEntered.Length == 0){
				current.path_ = true;
			}
			//else do this
			else{
				//if the word wasn't empty do this.
				for (int i = 0; i < wordedToBeEntered.Length; i++){
					//need to add a look up function here
                    LCTrieNode child = new LCTrieNode();
					 //current.children_.TryGetValue(wordedToBeEntered[i], out child);
					//move cursor forward if letter exist at postion
					if(current.children_.TryGetValue(wordedToBeEntered[i], out child)){
					  current = child;
					}
					else{
					  LCTrieNode tmp = new LCTrieNode();
					  current.children_.Add(wordedToBeEntered[i], tmp);
					  current = tmp;
					}
					if(i == wordedToBeEntered.Length - 1){
					  current.path_ = true;
					}
				}
			}
		}
		public Lexicon.Status LookUp(string wordToBeFound)
        {
			LCTrieNode  current = root_;
			int i = 0;
			Lexicon.Status found = Lexicon.Status.NOT_WORD;
			//if it is NULL do nothing
			//if(current != NULL){
				//this will break on the second pass after found is set
				do{
					//need to add look up function here.
					//temp = current.children_.find(wordToBeFound[i])->second;
					//if it comes back not true then it's set to null
                    if (!current.children_.TryGetValue(wordToBeFound[i], out current))
                    {
                        current = null;
                    }
					i++;
					//if i is bigger the word then the word was found and breaks
				}while (current != null &&  i < wordToBeFound.Length);
			//}
			//if it comes back not null then it might have been found	
			if( current != null){
			  //set to either false or true if the word exist.
				if(current.path_)
				{
					found = Lexicon.Status.WORD;
				}
				else
				{
					found = Lexicon.Status.WORD_PREFIX;
				}
			}
			else
			{
				//not a word do nothing
			}
			return found;
        }
    }
}
