﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BoggleBoardWeb.Startup))]
namespace BoggleBoardWeb
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
