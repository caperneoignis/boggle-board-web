﻿<%@ Page Title= "BoogleBoard" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="BoggleBoard.aspx.cs" Inherits="BoggleBoardWeb.BoggleBoard" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
        
        <asp:Panel ID="Panel1" runat="server" Height="840px" Width="840px" BorderStyle="Groove" ToolTip="Click the bottoms to move" ViewStateMode="Enabled">
        </asp:Panel>
        <asp:Button ID="checkBTN" runat="server" Text="Check Word" OnClick="Check_Click" Visible="True" Width="145px" />           
        <asp:Button ID="finishBTN" runat="server" Text="Finish" OnClick="doneBTN_Click" Visible="True" Width="116px" />
        <asp:Button ID="playAgain" runat="server" Text="Play Again" OnClick="playAgain_Click" Visible="True" Width="92px" />
        <p><asp:Label ID="humanScore" runat="server" Text="Your Score: "></asp:Label>
        <asp:Label ID="humanScoreDisplay" runat="server" Text="0" BorderStyle="Groove" CssClass="active" AssociatedControlID="checkBTN"></asp:Label></p>
        <p><asp:Label ID="ComputerScoreLabel" runat="server" Text="Computer Score: "></asp:Label>
            <asp:Label ID="computerScoreDisplay" runat="server" Text="0" BorderStyle="Groove" CssClass="active" AssociatedControlID="finishBTN"></asp:Label>
        </p>
</asp:Content>
    
