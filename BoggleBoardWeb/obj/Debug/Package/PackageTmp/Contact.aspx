﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="BoggleBoardWeb.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <h3>Contact Me:</h3>

    <address>
        <strong>Support:</strong>   <a href="mailto:bugreport@leesmosaic.com">bugreport@leesmosaic.com</a><br />
    </address>
</asp:Content>
