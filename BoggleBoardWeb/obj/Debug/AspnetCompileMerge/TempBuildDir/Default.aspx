﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="BoggleBoardWeb._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>Lee's Test Site</h1>
        <p class="lead">Lee's Boggle Board program and test site</p>
        <p><a href="BoggleBoard" class="btn btn-primary btn-large">Learn more &raquo;</a></p>
    </div>

    <div class="row">
        <div class="col-md-4">
            <h2>Working on ASP.net</h2>
            <p>
                I'm currently still debugging boggle board, but so far it's working well. 
            </p>
            <p>
                <a class="btn btn-default" href="BoggleBoard">Learn more &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Prospective Employers</h2>
            <p>
                Currently looking for employment. But in the mean time, I'll keep posting stuff to this site as I get it done. Hopefully this will showcase my talents. 
                ~LK
            </p>
            <p>
                <a class="btn btn-default" href="http://Leesmosaic.com">Learn more &raquo;</a>
            </p>
        </div>
        
    </div>

</asp:Content>
